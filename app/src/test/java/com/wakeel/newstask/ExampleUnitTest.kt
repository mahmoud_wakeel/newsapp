package com.wakeel.newstask

import com.wakeel.newstask.data.repository.NewsRepository
import com.wakeel.newstask.di.ViewModelModule
import com.wakeel.newstask.di.networkMockedComponent
import com.wakeel.newstask.di.repoMockedModule
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import retrofit2.HttpException
import java.io.File
import java.net.HttpURLConnection
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.koin.test.KoinTest
import org.koin.test.inject

@RunWith(JUnit4::class)
class ExampleUnitTest : KoinTest {
    private val newsRepo by inject<NewsRepository>()
    private lateinit var mockServer: MockWebServer

    @Before
    fun setUp() {
        mockServer = MockWebServer()
        mockServer.start()
        startKoin {
            modules(
                listOf(
                    ViewModelModule,
                    networkMockedComponent(mockServer.url("/").toString()),
                    repoMockedModule()
                )
            )
        }
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
        stopKoin()
    }

    private fun mockHttpResponse(responseCode: Int) = mockServer.enqueue(
        MockResponse()
            .setResponseCode(responseCode)
            .setBody(getJSON())
    )


    @Test
    fun get_news_result_ok() {
        mockHttpResponse(HttpURLConnection.HTTP_OK)
        runBlocking {
            val newsListMocked = newsRepo.fetchNewsWithPagination(getParameters())
            assertNotNull(newsListMocked)
            assertEquals(newsListMocked.isNullOrEmpty(), false)
        }
    }

    @Test(expected = HttpException::class)
    fun get_news_result_error() {
        mockHttpResponse(HttpURLConnection.HTTP_BAD_REQUEST)
        runBlocking {
            val newsListMocked = newsRepo.fetchNewsWithPagination(getParameters())
            assertEquals(newsListMocked.isNullOrEmpty(), true)
        }
    }

    private fun getParameters(): HashMap<String, String> {
        val parameters = HashMap<String, String>()
        parameters["page"] = "1"
        parameters["q"] = "all"
        return parameters
    }

    private fun getJSON(): String {
        val uri = this.javaClass.classLoader?.getResource("result_news_mocked.json")
        val file = File(uri!!.path)
        return String(file.readBytes())
    }
}
