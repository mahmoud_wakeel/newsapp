package com.wakeel.newstask

import android.app.Application
import com.wakeel.newstask.di.NetworkModule
import com.wakeel.newstask.di.RepoModule
import com.wakeel.newstask.di.ViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class NewsApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@NewsApplication)
            modules(listOf(NetworkModule, RepoModule, ViewModelModule))
        }
    }
}