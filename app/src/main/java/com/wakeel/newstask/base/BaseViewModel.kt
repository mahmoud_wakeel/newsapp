package com.wakeel.newstask.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel

open class BaseViewModel : ViewModel() {
    private val main = CoroutineScope(Dispatchers.Main)
    protected val io = CoroutineScope(Dispatchers.IO)

    override fun onCleared() {
        super.onCleared()
        main.coroutineContext.cancel()
        io.coroutineContext.cancel()
    }
}