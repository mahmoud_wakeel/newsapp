package com.wakeel.newstask.di

import com.wakeel.newstask.ui.viewmodel.NewsViewModel
import org.koin.dsl.module

val ViewModelModule = module {
    single {
        NewsViewModel(
            get()
        )
    }
}