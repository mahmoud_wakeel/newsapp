package com.wakeel.newstask.di

import com.wakeel.newstask.data.repository.NewsRepository
import org.koin.dsl.module

val RepoModule = module { single { NewsRepository(get()) } }