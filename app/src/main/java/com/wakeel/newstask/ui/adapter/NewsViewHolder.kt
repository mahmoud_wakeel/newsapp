package com.wakeel.newstask.ui.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.wakeel.newstask.R

class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val newsItem: ConstraintLayout = itemView.findViewById(R.id.news_item)
    val newsImg: ImageView = itemView.findViewById(R.id.news_img)
    val newsTitleTxt: TextView = itemView.findViewById(R.id.news_title_txt)
    val newsAuthorTxt: TextView = itemView.findViewById(R.id.news_author_txt)
    val newsDateTxt: TextView = itemView.findViewById(R.id.news_date_txt)
}