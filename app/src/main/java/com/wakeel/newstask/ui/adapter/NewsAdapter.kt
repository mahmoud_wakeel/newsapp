package com.wakeel.newstask.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.wakeel.newstask.R
import com.wakeel.newstask.data.model.Article
import com.wakeel.newstask.data.network.NetworkState

class NewsAdapter(private val newsListener: OnNewsClick, private val context: Context) :
    PagedListAdapter<Article, NewsViewHolder>(
        callback
    ) {
    private var currentNetworkState: NetworkState? = null

    companion object {
        private val callback = object : DiffUtil.ItemCallback<Article>() {
            override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.news_item, parent, false)
        return NewsViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.newsItem.setOnClickListener {
            newsListener.onNewsClick(getItem(position)!!)
        }
        Glide.with(context)
            .load(getItem(position)!!.urlToImage)
            .into(holder.newsImg)
        holder.newsTitleTxt.text = getItem(position)!!.title
        holder.newsAuthorTxt.text = getItem(position)!!.author
        holder.newsDateTxt.text = getItem(position)!!.publishedAt!!.substringBefore("T")
    }

    override fun getItemCount(): Int {
        this.newsListener.serviceState(super.getItemCount(), this.currentNetworkState)
        return super.getItemCount()
    }

    fun updateNetworkState(newNetworkState: NetworkState?) {
        val currentNetworkState = this.currentNetworkState
        val hadExtraRow = hasExtraRow()
        this.currentNetworkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        determineItemChange(hadExtraRow, hasExtraRow, currentNetworkState, newNetworkState)
    }

    private fun hasExtraRow() =
        currentNetworkState != null && currentNetworkState != NetworkState.SUCCESS

    private fun determineItemChange(
        hadExtraRow: Boolean, hasExtraRow: Boolean,
        currentNetworkState: NetworkState?,
        newNetworkState: NetworkState?
    ) {
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && currentNetworkState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    interface OnNewsClick {
        fun onNewsClick(article: Article)
        fun serviceState(size: Int, networkState: NetworkState?)
    }
}