package com.wakeel.newstask.ui.newsdetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.wakeel.newstask.R
import com.wakeel.newstask.data.model.Article
import kotlinx.android.synthetic.main.activity_news_details.*

const val News_Details = "news_details"

class NewsDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)
        val newsDetails = intent.getParcelableExtra<Article>(News_Details)
        setNewsDetails(newsDetails)
    }

    companion object {
        fun openNewsDetails(context: Context, article: Article) {
            val intent = Intent(context, NewsDetailsActivity::class.java)
            intent.putExtra(News_Details, article)
            context.startActivity(intent)
        }
    }

    private fun setNewsDetails(newsDetails: Article?) {
        if (newsDetails != null) {
            newsTitle.text = newsDetails.title
            newsAuthor.text = newsDetails.author
            newsDate.text = newsDetails.publishedAt?.substringBefore("T")
            newsDesc.text = newsDetails.description
            newsContent.text = newsDetails.content
            newsDetails.urlToImage?.let {
                Glide.with(this@NewsDetailsActivity)
                    .load(it)
                    .into(newsImg)
            }
        }
    }
}