package com.wakeel.newstask.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.wakeel.newstask.base.BaseViewModel
import com.wakeel.newstask.data.network.NetworkState
import com.wakeel.newstask.data.network.NewsDataSourceFactory
import com.wakeel.newstask.data.repository.NewsRepository

class NewsViewModel(newsRepository: NewsRepository) : BaseViewModel() {
    private val newsDataSourceFactory = NewsDataSourceFactory(newsRepository, io)
    val news = LivePagedListBuilder(newsDataSourceFactory, pagedListConfig()).build()
    val networkState: LiveData<NetworkState>? =
        Transformations.switchMap(newsDataSourceFactory.source) { it.getNetworkState() }
    fun loadNews() {
        if (news.value.isNullOrEmpty()) {
            refreshNews()
        }
    }

    fun refreshNews() {
        newsDataSourceFactory.getSource()?.refresh()
    }

    private fun pagedListConfig() = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()
}