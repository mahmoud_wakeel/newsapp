package com.wakeel.newstask.ui.news

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wakeel.newstask.R
import com.wakeel.newstask.data.model.Article
import com.wakeel.newstask.data.network.NetworkState
import com.wakeel.newstask.ui.adapter.NewsAdapter
import com.wakeel.newstask.ui.newsdetails.NewsDetailsActivity
import com.wakeel.newstask.ui.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(),
    NewsAdapter.OnNewsClick,
    SwipeRefreshLayout.OnRefreshListener {
    private val repositoryRecyclerViewAdapter =
        NewsAdapter(this, this@MainActivity)
    private val newsViewModel by viewModel<NewsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        swipe_container.setOnRefreshListener(this)
        swipe_container.post {
            swipe_container.isRefreshing = false
            newsViewModel.refreshNews()
        }
        newsViewModel.loadNews()
        newsList.adapter = repositoryRecyclerViewAdapter
        newsViewModel.networkState?.observe(
            this,
            Observer { repositoryRecyclerViewAdapter.updateNetworkState(it) })
        newsViewModel.news.observe(this, Observer {
            repositoryRecyclerViewAdapter.submitList(it)
        })
    }

    override fun onNewsClick(article: Article) {
        NewsDetailsActivity.openNewsDetails(this, article)
    }

    override fun serviceState(size: Int, networkState: NetworkState?) {
        warning_txt.visibility = GONE
        warning_img.visibility = GONE
        pBar.visibility = GONE
        when (size) {
            0 -> {
                warning_txt.text = getString(R.string.warning_txt)
                warning_txt.visibility = VISIBLE
                when (networkState) {
                    NetworkState.RUNNING -> {
                        pBar.visibility = VISIBLE
                        warning_txt.visibility = GONE
                    }
                    NetworkState.SUCCESS -> {
                        warning_txt.text = getString(R.string.warning_txt)
                        warning_txt.visibility = VISIBLE
                    }
                    NetworkState.FAILED -> {
                        warning_txt.text = getString(R.string.warning_txt)
                        warning_img.visibility = VISIBLE
                        warning_txt.visibility = VISIBLE
                    }
                }
            }
        }
    }

    override fun onRefresh() {
        newsViewModel.refreshNews()
        swipe_container.isRefreshing = false
    }
}
