package com.wakeel.newstask.data.repository

import com.wakeel.newstask.data.model.Article
import com.wakeel.newstask.data.network.NewsServices

class NewsRepository(private val newsServices: NewsServices) {
    private suspend fun fetchNews(params: HashMap<String, String>) =
        newsServices.fetchNewsAsync(params).await()

    suspend fun fetchNewsWithPagination(params: HashMap<String, String>): List<Article?>? {
        if (params.isEmpty()) return listOf()
        val request = fetchNews(params)
        return request.articles
    }
}