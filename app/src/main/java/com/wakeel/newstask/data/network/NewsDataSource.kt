package com.wakeel.newstask.data.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.wakeel.newstask.data.model.Article
import com.wakeel.newstask.data.repository.NewsRepository
import kotlinx.coroutines.*

class NewsDataSource(
    private val repository: NewsRepository,
    private val scope: CoroutineScope
) : PageKeyedDataSource<Int, Article>() {
    private var supervisorJob = SupervisorJob()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Article>
    ) {
        fetchNews(1) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        val page = params.key
        fetchNews(page) {
            callback.onResult(it, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {}

    private val networkState = MutableLiveData<NetworkState>()

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }


    private fun fetchNews(
        page: Int,
        callback: (List<Article>) -> Unit
    ) {
        networkState.postValue(NetworkState.RUNNING)
        scope.launch(getJobErrorHandler() + supervisorJob) {
            val news = repository.fetchNewsWithPagination(getParameters(page))
            networkState.postValue(NetworkState.SUCCESS)
            callback(news as List<Article>)
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, _ ->
        networkState.postValue(NetworkState.FAILED)
    }

    private fun getParameters(page: Int): HashMap<String, String> {
        val parameters = HashMap<String, String>()
        parameters["page"] = "$page"
        parameters["pageSize"] = "10"
        parameters["q"] = "all"
        parameters["apiKey"] = "b76e4e130f9e4b929eba5da73c28b9d6"
        return parameters
    }

    fun refresh() =
        this.invalidate()

    fun getNetworkState(): LiveData<NetworkState> =
        networkState

}