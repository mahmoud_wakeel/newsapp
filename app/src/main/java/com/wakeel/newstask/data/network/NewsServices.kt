package com.wakeel.newstask.data.network

import com.wakeel.newstask.data.model.News
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface NewsServices {
    @GET("/v2/everything")
    fun fetchNewsAsync(@QueryMap params: HashMap<String, String>): Deferred<News>
}