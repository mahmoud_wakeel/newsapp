package com.wakeel.newstask.data.network

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.wakeel.newstask.data.model.Article
import com.wakeel.newstask.data.repository.NewsRepository
import kotlinx.coroutines.CoroutineScope

class NewsDataSourceFactory(
    private val repository: NewsRepository,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, Article>() {

    val source = MutableLiveData<NewsDataSource>()

    fun getSource() = source.value

    override fun create(): DataSource<Int, Article> {
        val source = NewsDataSource(repository, scope)
        this.source.postValue(source)
        return source
    }
}