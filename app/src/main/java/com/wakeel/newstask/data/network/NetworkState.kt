package com.wakeel.newstask.data.network

enum class NetworkState {
    RUNNING,
    SUCCESS,
    FAILED
}